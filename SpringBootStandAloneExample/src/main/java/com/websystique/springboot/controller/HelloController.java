package com.websystique.springboot.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HelloController {

	@RequestMapping("/")
	String home(ModelMap modal) {
		modal.addAttribute("title", "Dear Learner");
		modal.addAttribute("message", "Welcome to SpringBoot");
		return "hello";
	}
	
	 @RequestMapping(value = "/persistPerson", method = RequestMethod.POST)
	    public ResponseEntity < String > persistPerson(@RequestBody StockDetailsDTO StockDetailsDTO) {
	       /* if (personService.isValid(person)) {
	            personRepository.persist(person);
	            return ResponseEntity.status(HttpStatus.CREATED).build();
	        }*/
	        return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT).build();
	    }
}
