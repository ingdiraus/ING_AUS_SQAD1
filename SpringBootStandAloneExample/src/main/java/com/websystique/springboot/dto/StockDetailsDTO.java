package com.websystique.springboot.dto;

public class StockDetailsDTO {

    private long id;
	
	private String username;
	
	private String stockname;
	
	private double stock_price;
	
	private int quantity;
	
	private double total_price;
	
	private double fees;
	
	private double final_price;
	
	private String stock_date;

	public StockDetailsDTO(){
		id=0;
	}
	
	
	
	public long getId() {
		return id;
	}



	public void setId(long id) {
		this.id = id;
	}



	public String getUsername() {
		return username;
	}



	public void setUsername(String username) {
		this.username = username;
	}



	public String getStockname() {
		return stockname;
	}



	public void setStockname(String stockname) {
		this.stockname = stockname;
	}



	public double getStock_price() {
		return stock_price;
	}



	public void setStock_price(double stock_price) {
		this.stock_price = stock_price;
	}



	public int getQuantity() {
		return quantity;
	}



	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}



	public double getTotal_price() {
		return total_price;
	}



	public void setTotal_price(double total_price) {
		this.total_price = total_price;
	}



	public double getFees() {
		return fees;
	}



	public void setFees(double fees) {
		this.fees = fees;
	}



	public double getFinal_price() {
		return final_price;
	}



	public void setFinal_price(double final_price) {
		this.final_price = final_price;
	}



	public String getStock_date() {
		return stock_date;
	}



	public void setStock_date(String stock_date) {
		this.stock_date = stock_date;
	}



	public StockDetailsDTO(long id, String username, String stockname,double stock_price,int quantity, double total_price,
			double fees, double final_price,String stock_date ){
		this.id = id;
		this.username = username;
		this.stockname = stockname;
		this.stock_price = stock_price;
		this.quantity = quantity;
		this.total_price = total_price;
		this.fees= fees;
		this.final_price=final_price;
		this.stock_date = stock_date;
	}
	
}
